app.controller('rolecontroller',['$scope','$http','$compile','$filter','$element','$location','getRoleListData','getUserMappedToRoleData','removeUserFromRole','getRoleData','remapUserData','editRoleData','getModulePageData','addRoleData','GetRolePermissionsData', function($scope,$http,$compile,$filter,$element,$location,getRoleListData,getUserMappedToRoleData,removeUserFromRole,getRoleData,remapUserData,editRoleData,getModulePageData,addRoleData,GetRolePermissionsData){
	
	$scope.showTable=false;
	$scope.pageName = $location.path();
	$scope.pageName = $scope.pageName.slice(1);
	$scope.selectedValue = {};
	$scope.selectedValueUser={};
	$scope.selectedValueRole={};
	
	$scope.selectedAll={};
	$scope.ActiveStatus = {};
	$scope.InactiveStatus = {};
	
	$scope.ViewCheckbox={};
	$scope.SubmitCheckbox={};
	$scope.ApproveCheckbox={};

	getRoleListData().success(function(response){
		$scope.roleList = response;
		$scope.showTable=true;
		
	}); 
	
	getModulePageData().success(function(response){
		$scope.ModulePageList = response;
		$scope.showTable=true;
		
	});
		
	$scope.editRole = function(data){
		console.log("here"+data);
		
	var editUserID = {
		id:data
	}		
	getRoleData(editUserID).success(function(response){	
	 
		$scope.roleEdit = response;
	 
	}); 
	}

	
	$scope.RemoveActiveRole=function(){
		$scope.roleInUse=null;
		$("#addNewRoles").trigger( "reset" );
		
	}


    $scope.removeUser=function(RoleId){
		
		$scope.removeUserList=[];
		$scope.removeUserDict=[];
		$scope.resultRemoveUserList=[];
        angular.forEach($scope.userList,function(value,key){
			if($scope.selectedValue[key+1]==true){
				
				$scope.removeUserList.push($scope.userList[key].pk_user_id);
			}
		});
		$scope.removeUserDict ={
		'RemoveDict':$scope.removeUserList
		}
		var resultRemoveUserList = {
		'RoleId' :1,
		'RemoveUserDict' : $scope.removeUserDict
		}
		
		$scope.buttonStyleApprove="buttondisable";
		$scope.buttonStyleReject="buttondisable";	
		
		$scope.removeData(resultRemoveUserList);
    }
	
	$scope.removeData = function(data){
		console.log(data);
		removeUserFromRole(data).success(function(){	
		alert("User Removed Successfully");
		getRoleListData().success(function(response){
		$scope.roleList = response;
		$scope.showTable=true;
		
	}); 
		}); 
	}


	$scope.remapUser=function(){
		var dict2=[];		
		angular.forEach($scope.userList,function(value,key1){
		 	if($scope.selectedValueUser[key1]==true){
				angular.forEach($scope.roleList,function(value,key2){
				if($scope.selectedValueRole[key2]==true){
					var UserID = $scope.userList[key1-1].pk_user_id;
					var RoleID = $scope.roleList[key2-1].pk_mst_role_id;
					dict1 = {
						'userID': UserID,
						'roleID': RoleID
					}					
					dict2.push(dict1);
				}
				});
			}
	    });
		$scope.remapData(dict2);
	}
	
	$scope.remapData = function(data){
		data = JSON.stringify(data)
		var dict = {
			final:data
		}
		
		remapUserData(dict).success(function(){	
			alert("User Remapped Successfully");
			getRoleListData().success(function(response){
		$scope.roleList = response;
		$scope.showTable=true;
		
	}); 
		}); 
	} 
	  
	$scope.editRoleSave=function(){
				
		dict1 = {       'roleID': $scope.roleEdit[0].pk_mst_role_id,
						'roleDescription':$scope.roleEdit[0].attr_role_description,
						'roleStatus':$scope.theBoolean
					}					
					
		$scope.editRoleData1(dict1);
	}
	 
		$scope.editRoleData1 = function(data){
			
		data = JSON.stringify(data)
		console.log(data);
		var dict = {
			final:data
		}
		
		editRoleData(dict).success(function(){	
			alert("Role Edited Successfully");
			getRoleListData().success(function(response){
		$scope.roleList = response;
		$scope.showTable=true;
		
	}); 
		}); 
	}
	
	$scope.checkAll = function (){
	
		if ($('.mastercheck').is(":checked")){
			angular.forEach($scope.userList,function(value,key){
			$scope.selectedValue[key+1]=false;
			$scope.selectedValue[key+1]=true;
			});		
		}
		else{		
			angular.forEach($scope.userList,function(value,key){
			$scope.selectedValue[key+1]=false;
			});
			
		}
		
	};
	
	$scope.uncheckMaster= function (checkboxVal){
		if(checkboxVal==false){
		if ($('.mastercheck').is(":checked")){
			$('.mastercheck').prop('checked', false);
		}
		}
	}
	
	$scope.addNewRolesHeader = ['ModuleName','PageName','View','Submit','Approve'];

	$scope.addRole=function()
	{
		var AddRole=[];	
		var PageDetail=[];
			angular.forEach($scope.ModulePageList,function(value,key2){
		 	
				//View
				if($scope.ViewCheckbox[key2+1])
				{		
			
					dict1 = {
						'ModuleID': $scope.ModulePageList[key2+1].pk_module_id,
						'PageID': $scope.ModulePageList[key2+1].pk_page_id,
						'Action': 'View',
							}					
				    PageDetail.push(dict1);
				}
				//Submit
				if($scope.SubmitCheckbox[key2+1])
				{		
					dict1 = {
						'ModuleID': $scope.ModulePageList[key2+1].pk_module_id,
						'PageID': $scope.ModulePageList[key2+1].pk_page_id,
						'Action': 'Submit',
							}					
				    PageDetail.push(dict1);
				}
				//Approve
				if($scope.ApproveCheckbox[key2+1])
				{		
					dict1 = {
						'ModuleID': $scope.ModulePageList[key2+1].pk_module_id,
						'PageID': $scope.ModulePageList[key2+1].pk_page_id,
						'Action': 'Approve',
							}					
				    PageDetail.push(dict1);
				}
			
	    });
		AddRole={'RoleName':$scope.RoleName,
				 'RoleDescription'	:$scope.RoleDescription,
				 'PageDetail':PageDetail			
				}
			
	$scope.addRoleData1(AddRole);
	
	};
	
	$scope.addRoleData1 = function(data){
		//console.log(data);
		 data = JSON.stringify(data)
		 console.log(data);
		var dict = {
			final:data
		}
		
		addRoleData(dict).success(function(){	
			alert("Role Added Successfully");
			
			
		}); 
	}
	
	//Manage Page Data
	$scope.editPageDetails = function(data){
		console.log("data"+data);
	if(data!=null)
	{
		var editUserID = {
			id:data
		}		
		GetRolePermissionsData(editUserID).success(function(response){	
		 GetRolePermissionsData
		 $scope.rolePageDetail = response;
		}); 
	}
	else
	{
		$scope.rolePageDetail=null;
		console.log("$scope.rolePageDetail"+$scope.rolePageDetail);
	}
	
	}
	
	$('#addNewRoles').on('hidden.bs.modal', function () {		
    $(this).find("input,textarea,select,checkbox").val('').end();
	$('.checkboxAction').prop('checked', false);
	$scope.rolePageDetail={};
	
});



$('#UserListModal').on('show', function(){
  console.log('The modal is show');
});
//--------------------------------------------------------------------------
//3
	$scope.HideReMap=function()
	{
		//to uncheck all checkboxes of the modal
		angular.forEach($scope.userList,function(value,key){
			$scope.selectedValue[key+1]=false;
		});	
		
		$('.checkboxAction').prop('checked', false);
		$( ".mastercheck" ).prop( "checked", false );
		if ($('.UserRadio').is(":checked")){
			$('.ReMapRadio').prop('checked', false);
			 $scope.Users = true;
			 $scope.ReMap = false;
		} 
	 
	}
	//3
	$scope.HideUsers=function()
	{
		//to uncheck all checkboxes of the modal
		angular.forEach($scope.userList,function(value,key){
			$scope.selectedValue[key+1]=false;
		});	
		
		$('.checkboxAction').prop('checked', false);
		$( ".mastercheck" ).prop( "checked", false );
		if ($('.ReMapRadio').is(":checked")){
			$('.UserRadio').prop('checked', false);
			 $scope.Users = false;
			 $scope.ReMap = true;
		}
	
	} 
$scope.roleInUse=null;
	//3
	$scope.roletoUser = function(data){
		
	
			$('.UserRadio').prop('checked', true);
			$('.ReMapRadio').prop('checked', false);
			 $scope.Users = true;
			 $scope.ReMap = false;
		
	
	$scope.roleInUse=data;	
	
	var editUserID = {
		id:data
	}		
	getUserMappedToRoleData(editUserID).success(function(response){	
	
		$scope.userList = response;
		
		
	}); 
	}
	
	$scope.CloseModal=function()
	{console.log("Closing Modal");
	$scope.rolePageDetail="";
	}
	
	$scope.Test=function()
	{
		console.log("Opening Modal");
		console.log("Before:"+$scope.rolePageDetail);
		$scope.rolePageDetail=null;
		console.log("After:"+$scope.rolePageDetail);
	}
	
	$scope.EnableRemove=function(){
		var flag=0;
	 angular.forEach($scope.userList,function(value,key){
		 if($scope.selectedValue[key+1]==true){
			 flag=1;
		 }
			});		
			if(flag==1)
			{
			$('.Remove').prop("disabled",false);
			}
			else
			{
			$('.Remove').prop("disabled",true);
			}
	}
	$scope.EnableRemap=function(){
		var flag1,flag2=0;
		angular.forEach($scope.userList,function(value,key){
			if($scope.selectedValue[key+1]==true){
				flag1=1;
			}
		});	
		angular.forEach($scope.roleList,function(value,key){
			 if($scope.selectedValueRole[key+1]==true){
				flag2=1;
			}
		});	
		if(flag1==1 && flag2==1){
			$('.Remap').prop("disabled",false);
		}
		else{
		$('.Remap').prop("disabled",true);
		}
	}
	
	
	$('#UserListModal').on('hidden.bs.modal', function () {	
	$('.checkboxAction').prop('checked', false)
	});
}]);

app.factory('getRoleListData', ['$http', function($http) { 
		return function(){
			return $http({
			 method: 'GET',
			 url:'http://192.168.0.109:8000/henkel/DisplayRoleUserMapping',
			});
			
		}
	}]);
	

app.factory('getUserMappedToRoleData', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/GetUsersForRole',
		  params:param,
		});
	}
}]);

app.factory('removeUserFromRole', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/RemoveUsersFromRole',
		  params:param,
		});
	}
}]);

app.factory('getRoleData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/DisplayRoleUserMapping',
		  params:param,
		});
	}
}]);

app.factory('remapUserData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/MapUsersToRole',
		  params:param,
		});
	}
}]);

//Edit Role
app.factory('editRoleData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/EditRole',
		  params:param,
		});
	}
}]);

//Populate ModuleName and PageName
app.factory('getModulePageData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/GetModulePageInformation',
		  params:param,
		});
	}
}]);

app.factory('addRoleData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/AddRole',
		  params:param,
		});
	}
}]);


app.factory('GetRolePermissionsData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/GetRolePermissions',
		  params:param,
		});
	}
}]);
