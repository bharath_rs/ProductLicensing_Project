app.controller('listusercontoller',['$scope','$rootScope','$http','$compile','$filter','$element','$location','addNewUserList','getUserListData','editNewUserList','$window','getRolesLanguageData','GetRoleUserInformation','updateUserData','sendActiveUserData', function($scope,$rootScope,$http,$compile,$filter,$element,$location,addNewUserList,getUserListData,editNewUserList,$window,getRolesLanguageData,GetRoleUserInformation,updateUserData,sendActiveUserData){

	$scope.iframeHeight = $(window).height();		// Total Window Height eg.662
	$scope.imageHeight = $('.image').height();		// Header Height eg.55	
	$scope.headerHeight = $('.headerdiv').height();	// Header Height eg.30		
	$scope.footerHeight = $('#footer').height();	// Header Height eg.39		
	$scope.bodyHeight = $scope.iframeHeight - ($scope.imageHeight + $scope.headerHeight + $scope.footerHeight);		//Total Body Height eg.573

	$scope.pageName = $location.path();
	$scope.pageName = $scope.pageName.slice(1);
	
	/* $("select").multiselect({maxHeight: 250, includeSelectAllOption: true,enableFiltering: true,disableIfEmpty: true
		,nonSelectedText: 'Select '
		,enableCaseInsensitiveFiltering: true
		
	}); */
	
	
	
	$scope.selectedValue = {};
	$scope.buttonStyleApprove = "buttondisable";
	$scope.buttonStyleReject ="buttondisable";	
	
	

	
    $("#form").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
			"username": {
                required: true,
            },
            "email": {
                required: true,
                email: true
            },
			"firstname": {
				required: true
			},
			"lastname": {
				required: true
			}
        },
        messages: {
            "name": {
                required: "Please, enter a name"
            },
			"username": {
                required: "Please, enter a name"
            },
            "email": {
                required: "",
                email: "Email is invalid"
            },
			"firstname": {
				required: ""
			},
			"lastname": {
				required: ""
			}
        },
        submitHandler: function (form) { 
				
			$scope.update = function(user) {
				
				var selectedUserType = $("#userradio input[type='radio']:checked").val();
				
				var type = $('#add').html();
				$scope.finalData = [];
				$scope.master = angular.copy(user); 
				
				
				if($scope.master.usertype == undefined){
					$scope.master.usertype = selectedUserType
				}
				console.log($scope.master.usertype);
				//console.log('master',$scope.master);
				$scope.roles = $scope.master.roles;
				$scope.selectedRoles=[];
				angular.forEach($scope.roles, function(value, key) {
					if(value == true){				
						$scope.selectedRoles.push(key);
					}					
				});
				$scope.finalData.push($scope.master);
				$scope.finalData.push($scope.Language);
				$scope.finalData.push($scope.selectedRoles);
				var rolesDict = {
					'role':$scope.selectedRoles
				}
				var finalDataDict = {
					'First':$scope.master,
					'Language':$scope.Language,
					'Roles':rolesDict
				};
				console.log('final',finalDataDict); 
				
				if(type == "Add New User"){
					
					addNewUserList(finalDataDict).success(function(response){
						console.log("Success");
					});
					
					angular.element('#addNewUserModal').modal('hide');		// Close Modal
				}
				if(type != "Add New User"){   //updateUserData
				
					//console.log(finalDataDict);
					
					updateUserData(finalDataDict).success(function(response){
						console.log("Success");
					});
					angular.element('#addNewUserModal').modal('hide');		// Close Modal
					
				}				
				
				 						
				
			};
           return false; 
        }
    });
	
	$scope.getUserDetails = function(data){
		angular.forEach($rootScope.RolesListGlobal, function(value, key){			
			$('#'+value.replace(/\s+/g, '')+'').prop('checked', false);
		});
		var editUserID = {
			id:data
		}		
		editNewUserList(editUserID).success(function(response){
			$scope.edituser = response;	
			//console.log($scope.edituser);
		}); 
		console.log('ID',editUserID);
		GetRoleUserInformation(editUserID).success(function(response){  
			console.log(response);
			if(response.length != 0){
				$('#'+response[0].attr_user_type+'').prop('checked', true);
				$('#languageSelect').val(response[0].attr_language_name);								// Setting 'Language'
				$scope.selectedRolesList = [];
				angular.forEach(response, function(value, key) {	
					$scope.selectedRolesList.push(value.attr_mst_role_name.replace(/\s+/g, ''));		//remove the space between the words
					$('#'+value.attr_mst_role_name.replace(/\s+/g, '')+'').prop('checked', true);		// Setting 'Roles'
				});				
			}
						
		}); 
		
	}
	
	getUserListData().success(function(response){
		$scope.userList = response;
	}); 
	
	getRolesLanguageData().success(function(response){							// Populate Roles and Languages in the Modal
		$scope.RolesLanguage = response;
		$scope.RolesList = [];
		$rootScope.RolesListGlobal = [];
		$scope.LanguageList = [];
		angular.forEach($scope.RolesLanguage, function(value, key) {
			if(value.attr_mst_role_name != undefined){
				$scope.RolesList.push(value.attr_mst_role_name);
				$rootScope.RolesListGlobal.push(value.attr_mst_role_name);
			}
			if(value.attr_language_name != undefined){
				$scope.LanguageList.push(value.attr_language_name);
			}						
		});	
		
		str = '<select id="languageSelect" data-live-search="true" class="selectBox">';	
		for(i=0;i<$scope.LanguageList.length;i++){
			str = str + '<option ng-value="$scope.LanguageList[i]">'+$scope.LanguageList[i]+'</option>';
		}		
		str = str + '</select>';				
		angular.element($('.languagemenu')).append($compile(str)($scope));
		/* $("#languageSelect").multiselect({maxHeight: 250, includeSelectAllOption: true,enableFiltering: true,disableIfEmpty: true
			,nonSelectedText: 'Select '
			,enableCaseInsensitiveFiltering: true
			
		});	 */
		$scope.Language = '';
		$scope.Language = $('#languageSelect').val();
		
		
		$('#languageSelect').on('change', function(){
			var selected = $(this).find("option:selected");
			var arrSelected = [];
			selected.each(function(){
			   $scope.Language = $(this).val();
			   console.log('Selected',$scope.Language);
			});
		});
	}); 
		

	$scope.editUsers = function(){
		$( "#firstname" ).prop( "disabled", false );
		$( "#lastnameedit" ).prop( "disabled", false );
		$( "#email" ).prop( "disabled", false );
	}
			
		
	$scope.checkAll = function (){
		if ($('.mastercheck').is(":checked")){
			angular.forEach($scope.userList,function(value,key){
			$scope.selectedValue[key+1]=true;
			});		
		}
		else{		
			angular.forEach($scope.userList,function(value,key){
			$scope.selectedValue[key+1]=false;
			});
		}
		$scope.checkActiveUser();
	};		
 
    $scope.ClearSearchText=function(){
	
		$scope.FirstName = null;
		$scope.search.LastName = null;
		$scope.search.LoginName = null;
	}
 
	$scope.changeClass = function(){
		if ($scope.class === "activatebutton")
			$scope.class = "deactivatebutton";
		else
		$scope.class = "activatebutton";
	};

	$scope.checkActiveUser=function(checkboxVal) {
	if(checkboxVal==false){
		if ($('.mastercheck').is(":checked")){
			$('.mastercheck').prop('checked', false);
		}
		}
		var flagActivate=false,flagDeactivate=false,lastactive="none"; 
		$scope.buttonStyleApprove="buttondisable";
		$scope.buttonStyleReject="buttondisable";	
		
		$('.btnActivate').prop("disabled",true);
		$('.btnDeactivate').prop("disabled",true);
		
		angular.forEach($scope.userList,function(value,key){
			if($scope.userList[key].attr_status==true && $scope.selectedValue[key+1]==true){
				flagDeactivate=true;  
				lastactive="Active";
			}
			if($scope.userList[key].attr_status==false && $scope.selectedValue[key+1]==true){
				flagActivate=true;
				lastactive="Inactive";
			}
		});

		if(flagDeactivate==true && flagActivate==false){
			$scope.buttonStyleReject="rejectBtn";
			$scope.buttonStyleApprove="buttondisable";
			$('.btnActivate').prop("disabled",true);
			$('.btnDeactivate').prop("disabled",false);
			
		}
		
		if(flagActivate==true && flagDeactivate==false){
			$scope.buttonStyleApprove="approveBtn";
			$scope.buttonStyleReject="buttondisable";
			$('.btnActivate').prop("disabled",false);
			$('.btnDeactivate').prop("disabled",true);
		}
		if(flagDeactivate==true && flagActivate==true){
			$scope.buttonStyleApprove="buttondisable";
			$scope.buttonStyleReject="buttondisable";
			$('.btnActivate').prop("disabled",true);
			$('.btnDeactivate').prop("disabled",true);
		}
				
	}

	/* ------------ Activate Users and deactivate Users ------------------*/
	  
	$scope.activateUsers=function() {
		 $scope.activateUserList=[];
		 $scope.activateUserDict=[];
		 $scope.resultActivateUserList=[];

		 angular.forEach($scope.userList,function(value,key){
		 if($scope.selectedValue[key+1]==true && $scope.userList[key].attr_status==false){
			   $scope.activateUserList.push($scope.userList[key].pk_user_id);
			   }
		});
			
		$scope.activateUserDict ={
		'statusDict':$scope.activateUserList
		}
			
		var resultActivateUserList = {
			'Status' : true,
			'Dict' : $scope.activateUserDict
		}
			
		$scope.buttonStyleApprove="buttondisable";
		$scope.buttonStyleReject="buttondisable";	
		$scope.sendData(resultActivateUserList);
		
	} 	
	$scope.sendData = function(data){
		console.log(data);
				
		sendActiveUserData(data).success(function(){	
			alert("Status Updated");
			
		}); 
		
	}	
	$scope.deactivateUsers=function() {
		$scope.deactivateUserList=[];
		$scope.deactivateUserDict=[];
		$scope.resultDeactivateUserList=[];

		angular.forEach($scope.userList,function(value,key){
			if($scope.selectedValue[key+1]==true && $scope.userList[key].attr_status==true){ 
				$scope.deactivateUserList.push($scope.userList[key].pk_user_id);
			}
		});
		$scope.deactivateUserDict ={
			'statusDict':$scope.deactivateUserList
		}
			
		var resultDeactivateUserList = {
			'Status' : false,
			'Dict' : $scope.deactivateUserDict
		}			
		$scope.buttonStyleApprove="buttondisable";
		$scope.buttonStyleReject="buttondisable";	
		$scope.sendData(resultDeactivateUserList);
		
	}	
	
	/* -------------- End Activate Users and deactivate Users ----------------------*/
 	

	$scope.ClearSearchText=function(){
		$scope.search.attr_first_name = null;
		$scope.search.attr_last_name = null;
		$scope.search.attr_email_id = null;
	}	
	
}]);

app.directive('myBreadCrumb', function(){
	return{
		template: 'User Management <i class="fa fa-angle-double-right" aria-hidden="true"></i> {{pageName}}'
	}
});

app.factory('addNewUserList', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/AddUser',
		  params:param,
		});
	}
}]);
app.factory('GetRoleUserInformation', ['$http', function($http) { 
	return function(param){
		return $http({
		 method: 'GET',
		 url:'http://192.168.0.109:8000/henkel/GetRoleUserInformation',
		 params:param,
		});
	}
}]);
app.factory('editNewUserList', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/DisplayUserQuery',
		  params:param,
		});
	}
}]);
app.factory('getUserListData', ['$http', function($http) { 
	return function(){
		return $http({
		 method: 'GET',
		 url:'http://192.168.0.109:8000/henkel/DisplayUserQuery',
		});
	}
}]);
app.factory('getRolesLanguageData', ['$http', function($http) { 
	return function(){
		return $http({
		 method: 'GET',
		 url:'http://192.168.0.109:8000/henkel/GetLanguagesAndRoles',
		});
	}
}]);
app.factory('updateUserData', ['$http', function($http) { 
	return function(param){
		return $http({
		 method: 'GET',
		 url:'http://192.168.0.109:8000/henkel/EditUser',
		 params:param,
		});
	}
}]);
app.factory('sendActiveUserData', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/ActivateDeactivateUser',
		  params:param,
		});
	}
}]);