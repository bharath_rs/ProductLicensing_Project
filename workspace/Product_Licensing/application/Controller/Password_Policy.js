app.controller('pwdPolicyController',['$scope','$http','$compile','$filter','$element','$location','getPasswordPolicy','editPasswordPolicy', function($scope,$http,$compile,$filter,$element,$location,editPasswordPolicy,getPasswordPolicy){
	
	$scope.dictPasswordPolicy=[];
	
	/*getPasswordPolicy().success(function(response){
		console.log(response) ;
	}); */
	
	$scope.Update=function(){
		
		if($scope.minLen==($scope.minDig+$scope.minSym+$scope.minUpperChar+$scope.minAlpha))
		{
			$scope.dictPasswordPolicy ={
					'minLen':$scope.minLen,
					'graceperiod':$scope.graceperiod,
					'minDig':$scope.minDig,
					'forcepwddays':$scope.forcepwddays,
					'minSym':$scope.minSym,
					'expDays':$scope.expDays,
					'minUpperChar':$scope.minUpperChar,
					'expAlertDays':$scope.expAlertDays,
					'minAlpha':$scope.minAlpha,
					'failedAttempts':$scope.failedAttempts
			}
			$scope.updatePasswordPolicy($scope.dictPasswordPolicy);
		}
		else
			{
			$scope.validation="validation";
			}
	}
	$scope.updatePasswordPolicy = function(data){
		data = JSON.stringify(data)
		console.log(data);
	/*	var dict = {
			final:data
		}
		editPasswordPolicy(dict).success(function(){	
			alert("Password Policy Updated Successfully");
		});*/
	} 
	
	$scope.Reset=function(){
		$scope.validation="";
		$scope.minLen=0;
		$scope.graceperiod=0;
		$scope.minDig=0;
		$scope.forcepwddays=0;
		$scope.minSym=0;
		$scope.expDays=0;
		$scope.minUpperChar=0;
		$scope.expAlertDays=0;
		$scope.minAlpha=0;
		$scope.failedAttempts=0;
	}

}]);

//GetPasswordPolicy
app.factory('getPasswordPolicy', ['$http', function($http) { 
	return function(param){
	return $http({
	  method: 'GET',
	  url:'http://192.168.0.109:9000/henkel/PasswordPolicyGet',
	  params:param,
	});
}
}]);

//Update Password Policy
app.factory('editPasswordPolicy', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:9000/henkel/PasswordPolicyUpdate',
		  params:param,
		});
	}
}]);