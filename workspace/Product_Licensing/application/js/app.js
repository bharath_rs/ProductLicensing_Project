var app = angular.module('app', ['ngRoute','datatables']);

//var app=angular.module("app",['ngRoute','datatables','ngMap',"ngStorage",'timer','angular.filter','angular-loading-bar','ui.bootstrap']);

app.controller('controller1',['$scope','$http','$compile','$filter','$element','$location', function($scope,$http,$compile,$filter,$element,$location){

	$scope.iframeHeight = $(window).height();		// Total Window Height eg.662
	$scope.imageHeight = $('.image').height();		// Header Height eg.55	
	$scope.headerHeight = $('.headerdiv').height();	// Header Height eg.30		
	$scope.footerHeight = $('#footer').height();	// Header Height eg.39		
	$scope.bodyHeight = $scope.iframeHeight - ($scope.imageHeight + $scope.headerHeight + $scope.footerHeight);	//Total Body Height eg.573
	//Naveen
	$scope.hometab =	[{
							'Management':['Manage User','Manage Role','Password Policy'],							
						},
						{
							'Configuration':['aa','bb','cc']
						}];
						
				
	$scope.translate = [
	{
		Element_ID:'1',
		Language_ID:'1',
		AttrDescription: 'Manage User',
	},
	{
		Element_ID:'1',
		Language_ID:'2',
		AttrDescription: "Gérer l'utilisateur'",
	}
	]
	
						
	var len = Object.keys($scope.hometab).length;	
	$scope.pageName = $location.path();
	$scope.pageName = $scope.pageName.slice(1);
	
	
}]);

//Router to navigate to different pages in the application
app.config(function($routeProvider) {
	debugger
	$routeProvider		
	.when("/Manage User", {
		templateUrl : "application/templates/userslist.html"
			//,controllerUrl  : "application/Controller/userlist.js"  
	})
	.when("/Manage Role", {
		templateUrl : "application/templates/role.html"
			//,	controllerUrl  : "application/Controller/role.js"  
	})
	.when("/Password Policy", {
		templateUrl : "application/templates/Password_Policy.html"
		//	,		controllerUrl  : "application/Controller/Password_Policy.js"  
	});
});		





