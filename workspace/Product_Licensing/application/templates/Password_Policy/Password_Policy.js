app.controller('pwdPolicyController',['$scope','$http','$compile','$filter','$element','$location','getRoleListData','getUserMappedToRoleData','removeUserFromRole','getRoleData','remapUserData','editRoleData', function($scope,$http,$compile,$filter,$element,$location,getRoleListData,getUserMappedToRoleData,removeUserFromRole,getRoleData,remapUserData,editRoleData){
	
	$scope.showTable=false;
	console.log($scope.showTable);
	$scope.pageName = $location.path();
	$scope.pageName = $scope.pageName.slice(1);
	$scope.selectedValue = {};
	$scope.selectedValueUser={};
	$scope.selectedValueRole={};
	
	getRoleListData().success(function(response){
		$scope.roleList = response;
		$scope.showTable=true;
		
	}); 
	
	angular.module('showcase', ['datatables']);
	
	
	$scope.roleInUse=null;
	$scope.roletoUser = function(data){
	$scope.roleInUse=data;	
	
	var editUserID = {
		id:data
	}		
	getUserMappedToRoleData(editUserID).success(function(response){	
	
		$scope.userList = response;
		
		
	}); 
	}
	
	
	$scope.editRole = function(data){
		
	var editUserID = {
		id:data
	}		
	getRoleData(editUserID).success(function(response){	
	 
		$scope.roleEdit = response;
	 
	}); 
	}

	$(function() {
		/*$("#roleList").dataTable().fnDestroy();
		$('#roleList').DataTable({
		'iDisplayLength': 4,
		"oLanguage": { "sSearch": "" }
		});	
		
		$('.dataTables_filter input').css("display", "none");
		$('#example').DataTable({
			"searching": false,
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"lengthChange": false,
			"bInfo": false,
			"bAutoWidth": false,
			 "showNEntries" : false
		});*/
	});
	
	
	$scope.RemoveActiveRole=function(){
		$scope.roleInUse=null;
	}
	

    $scope.removeUser=function(RoleId){
		
		$scope.removeUserList=[];
		$scope.removeUserDict=[];
		$scope.resultRemoveUserList=[];
        angular.forEach($scope.userList,function(value,key){
			if($scope.selectedValue[key+1]==true){
				
				$scope.removeUserList.push($scope.userList[key].pk_user_id);
			}
		});
		$scope.removeUserDict ={
		'RemoveDict':$scope.removeUserList
		}
		var resultRemoveUserList = {
		'RoleId' :1,
		'RemoveUserDict' : $scope.removeUserDict
		}
		
		$scope.buttonStyleApprove="buttondisable";
		$scope.buttonStyleReject="buttondisable";	
		
		$scope.removeData(resultRemoveUserList);
    }
	
	$scope.removeData = function(data){
		removeUserFromRole(data).success(function(){	
		alert("User Removed Successfully");
		}); 
	}


	$scope.remapUser=function(){
		var dict2=[];		
		angular.forEach($scope.userList,function(value,key1){
		 	if($scope.selectedValueUser[key1]==true){
				angular.forEach($scope.roleList,function(value,key2){
				if($scope.selectedValueRole[key2]==true){
					var UserID = $scope.userList[key1-1].pk_user_id;
					var RoleID = $scope.roleList[key2-1].pk_mst_role_id;
					dict1 = {
						'userID': UserID,
						'roleID': RoleID
					}					
					dict2.push(dict1);
				}
				});
			}
	    });
		$scope.remapData(dict2);
	}
	
	$scope.remapData = function(data){
		data = JSON.stringify(data)
		var dict = {
			final:data
		}
		
		remapUserData(dict).success(function(){	
			alert("User Remapped Successfully");
		}); 
	} 
	  
	$scope.editRoleSave=function(){
		// alert("here");
		// alert($scope.roleEdit[0].pk_mst_role_id);
		// alert($scope.roleEdit[0].attr_role_description);
		// alert($scope.roleEdit[0].attr_status);
		if($scope.item.checkbox==true)
		{
			alert("Active");
		}
	
	}
	  
	$scope.HideReMap=function()
	{
		debugger;
	$('#UserRadio').prop('checked',true);
	   $scope.Users = true;
	   $scope.ReMap = false;
	 
	 
	}
	$scope.HideUsers=function()
	{
	
	 $('#RemapRadio').prop('checked',true);
	   $scope.Users = false;
	    $scope.ReMap = true;
	  
	} 


			
	$scope.checkAll = function (){
		if($scope.selectedAll==true){
		  	angular.forEach($scope.userList,function(value,key){
				$scope.selectedValue[key+1]=true;
			});

		}
		else
		{
				angular.forEach($scope.userList,function(value,key){
				$scope.selectedValue[key+1]=false;
			});
		}
	};
	
	$scope.addNewRolesHeader = ['ModuleName','PageName','View','Submit','Approve'];

	$scope.addNewList = [
		{
			'ModuleName': 'Configuration',
			'PageName':'Dashboard',
			'View': 'a',
			'Submit': 'b',
			'Approve':'c'
		},
		{
			'ModuleName': 'Configuration',
			'PageName':'Layout',
			'View': 'a',
			'Submit': 'b',
			'Approve':'c'
		},
		{
			'ModuleName': 'Configuration',
			'PageName':'Report',
			'View': 'a',
			'Submit': 'b',
			'Approve':'c'
		},
		{
			'ModuleName': 'Target',
			'PageName':'Target Screen',
			'View': 'a',
			'Submit': 'b',
			'Approve':'c'
		},
		{
			'ModuleName': 'Target',
			'PageName':'Manage Users',
			'View': 'a',
			'Submit': 'b',
			'Approve':'c'
		},
		{
			'ModuleName': 'Target',
			'PageName':'Add New User',
			'View': 'a',
			'Submit': 'b',
			'Approve':'c'
		}]; 
			
}]);

app.factory('getRoleListData', ['$http', function($http) { 
		return function(){
			return $http({
			 method: 'GET',
			 url:'http://192.168.0.109:8000/henkel/DisplayRoleUserMapping',
			});
			
		}
	}]);
	

app.factory('getUserMappedToRoleData', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/GetUsersForRole',
		  params:param,
		});
	}
}]);

app.factory('removeUserFromRole', ['$http', function($http) { 
	return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:8000/henkel/RemoveUsersFromRole',
		  params:param,
		});
	}
}]);

app.factory('getRoleData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:9000/henkel/DisplayRoleUserMapping',
		  params:param,
		});
	}
}]);

app.factory('remapUserData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:9000/henkel/MapUsersToRole',
		  params:param,
		});
	}
}]);

//Edit Role
app.factory('editRoleData', ['$http', function($http) { 
		return function(param){
		return $http({
		  method: 'GET',
		  url:'http://192.168.0.109:9000/henkel/MapUsersToRole',
		  params:param,
		});
	}
}]);